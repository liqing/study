package com.squareup.otto;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;


public class XXEventBus extends Bus {
    private static XXEventBus ourInstance = new XXEventBus(ThreadEnforcer.ANY);

    public static XXEventBus getInstance() {
        return ourInstance;
    }

    public XXEventBus(ThreadEnforcer enforcer) {
        super(enforcer, "Indy-Ble-LowLevel");
    }
}
