package com.liqing.util;

import android.util.Log;
/**
 * 
 * <pre>    
 * 项目名称：com.liqing.util.jar    
 * 类名称：CLog    
 * 类描述：    
 * 创建人：liqing    
 * 创建时间：2015年6月23日 下午5:55:37    
 * 修改人：liqing    
 * 修改时间：2015年6月23日 下午5:55:37    
 * 修改备注：    
 * @version     
 * </pre>
 */
public class CLog {
    private static boolean IS_TEST = true;

    public static void i(String pTag, String pMessage) {
        if (IS_TEST)
            Log.i(pTag, "++++" + pMessage + "++++");
    }

    public static void e(String pTag, String pMessage) {
        if (IS_TEST)
            Log.e(pTag, "++++" + pMessage + "++++");
    }

    public static void d(String pTag, String pMessage) {
        if (IS_TEST)
            Log.d(pTag, "++++" + pMessage + "++++");

    }
}
