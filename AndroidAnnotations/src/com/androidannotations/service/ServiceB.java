package com.androidannotations.service;

import org.androidannotations.annotations.EIntentService;

import android.app.IntentService;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

@EIntentService
public class ServiceB extends IntentService {

    public ServiceB() {
        super(ServiceB.class.getSimpleName());
    }

    private String TAG = "ServiceB";

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e(TAG, "++oncreate++");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "++onstartcommand++now thread is " + Thread.currentThread().getName());
        return super.onStartCommand(intent, flags, startId);
    }
    
    @Override
    protected void onHandleIntent(Intent intent) {
        Log.e(TAG, "++onHandleIntent++now thread is " + Thread.currentThread().getName());
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "++onDestory++");
    }
}
