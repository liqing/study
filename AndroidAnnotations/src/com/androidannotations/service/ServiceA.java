package com.androidannotations.service;

import org.androidannotations.annotations.EService;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

@EService
public class ServiceA extends Service{

    private String TAG="ServiceA";
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        Log.e(TAG, "++oncreate++");
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "++onstartcommand++now thread is "+Thread.currentThread().getName());
        return super.onStartCommand(intent, flags, startId);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "++onDestory++");
    }
}
