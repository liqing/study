package com.androidannotations.activity;

import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EActivity;

import com.androidannotations.AppTest;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

@EActivity
public class ActivityBase extends Activity {
    private String TAG = "ActivityBase";

    @App
    AppTest mApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(TAG, "++oncreate++");
    }
}
