package com.androidannotations.activity;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.androidannotations.R;
import com.androidannotations.activity.ActivitySecond_.IntentBuilder_;
import com.androidannotations.service.ServiceA_;
import com.androidannotations.service.ServiceB_;

@EActivity(R.layout.activity_main)
public class MainActivity extends ActivityBase {

    @ViewById(R.id.idBut1)
    Button mButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Click(R.id.idBut1)
    void goToAntherActivity() {
        IntentBuilder_ _IntentBuilder = ActivitySecond_.intent(this);
        Intent _Intent = _IntentBuilder.get();
        _Intent.putExtra("test", 1);
        _IntentBuilder.start();
    }

    @Click(R.id.idBut2)
    void startAservice() {
        ServiceA_.intent(getApplicationContext()).start();
    }

    @Click(R.id.idBut3)
    void startBservice() {
        ServiceB_.intent(getApplicationContext()).start();
    }

    @Click(R.id.idBut4)
    void stopAservice() {
        ServiceA_.intent(getApplicationContext()).stop();
    }
}
