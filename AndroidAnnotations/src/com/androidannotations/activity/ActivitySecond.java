package com.androidannotations.activity;

import org.androidannotations.annotations.EActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;


@EActivity
public class ActivitySecond extends ActivityBase{
    private String TAG="ActivitySecond";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent _Intent = getIntent();
        if (_Intent!=null) {
            int x = _Intent.getIntExtra("test", 0);
            Log.e(TAG, x+"===");
        }
    }
}
