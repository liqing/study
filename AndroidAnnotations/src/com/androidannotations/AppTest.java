package com.androidannotations;

import org.androidannotations.annotations.EApplication;

import com.squareup.otto.XXEventBus;

import android.app.Application;

@EApplication
public class AppTest extends Application {
    XXEventBus mEventBus = XXEventBus.getInstance();
    @Override
    public void onCreate() {
        super.onCreate();
    }
}
