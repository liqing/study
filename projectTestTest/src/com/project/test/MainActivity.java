package com.project.test;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.util.EncodingUtils;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends Activity {
	/**
	 * sd????
	 */
	public static final String PATH_BASE = Environment
			.getExternalStorageDirectory().getAbsolutePath();
	String _path = PATH_BASE + "/";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		try {
			Log.e("-->", ReadFromFile.readFileSdcardFile(_path + "copy.txt"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String fileName = "copy.txt"; // ???????
		String res = "";
		try {

			// ???????е?asset?????
			InputStream in = getResources().getAssets().open(fileName);

			int length = in.available();
			byte[] buffer = new byte[length];

			in.read(buffer);
			in.close();
			res = EncodingUtils.getString(buffer, "UTF-8");
			Log.e("-->", "???????" + length + "-->" + res);
			int _n256 = length / 256;
			
			for (int i = 0; i < _n256; i++) {
				Log.e("-->", res.substring(i*256, i*256+256));
			}
			int _m256 = length%256;
			if (_m256!=0) {
				Log.e("--->", res.substring(256*_n256, length));
			}
			
		} catch (Exception e) {

			e.printStackTrace();

		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
